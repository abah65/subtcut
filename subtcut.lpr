program subtcut;

{$mode objfpc}{$H+}

uses
      {$IFDEF UNIX}{$IFDEF UseCThreads}
      cthreads,
      {$ENDIF}{$ENDIF}
      Classes, SysUtils, FileUtil, Process, fpjson, jsonparser,
      CustApp, LConvEncoding, Variants, DateUtils;

type

      { TSubtcut }

      TParams = record
        sTarget, sTime, sBackup, sAction, sTemp: string;
        vStart, vEnd: Variant;
        bRecursive: boolean;
			end;

      TApp = record
        sAppDir, sMerge, sExtract: string;
			end;

      TSubtcut = class(TCustomApplication)
      protected
            procedure DoRun; override;
            procedure ExecOnDir(sTargetLC: string); virtual;
            //procedure ExecOnOthers; virtual;
            procedure ExecOnMKV; virtual;
            procedure ExecOnSrt(sTarget: string; vStart: variant; vEnd: variant; sBackup: string; sAction: string); virtual;
            procedure StopRun(Msg: string; iExitCode: integer); virtual;
            function DblQ(s: string; setquote:boolean = true): string; virtual;
            function StrSec(vInput: variant): variant;
            function Rand(chars: integer = 16): string;
      public
            constructor Create(TheOwner: TComponent); override;
            destructor Destroy; override;
            procedure ShowHelp; virtual;
      end;

const
  MKVSubType = 'tracks[*].codec';
  MKVSubName = 'tracks[*].properties.track_name';
  MKVSubLang = 'tracks[*].properties.language';
  MKVSubDefault = 'tracks[*].properties.default_track';

var
      Application: TSubtcut;
      Param: TParams;
      App: TApp;

{ TSubtcut }

{random string generator}
function TSubtcut.Rand(chars: integer = 16): string;
var s: string;
begin
      Randomize;
      s:= IntToHex(Random(Int64($7fffffffffffffff)), 16);
      result:= LeftStr(s,chars);
end;

procedure TSubtcut.StopRun(Msg: string; iExitCode: integer);
  begin
    writeln(Msg);
    halt(iExitCode);
  end;

function TSubtcut.DblQ(s: string; setquote:boolean = true): string;
begin
      if setquote then result:='"'+s.Trim+'"'
      else result:=s.Replace('"','');
end;

function TSubtcut.StrSec(vInput: variant): variant;
begin
     Case varType(vInput) of
     varString:
       begin
       try result:= MilliSecondOfTheDay(StrToTime(vInput));
		   except StopRun('Invalid time: ' + vInput, 1);
		   end;
	   end;
     varInteger:
       begin
       try result:= FormatDateTime('hh:nn:ss.zzz', vInput / MSecsPerDay);
		   except StopRun('Invalid time: ' + vInput, 1);
		   end;
	   end;
     else
       StopRun('Invalid time: ' + vInput, 1);
	 end;
end;

procedure TSubtcut.ExecOnSrt(sTarget: string; vStart: variant; vEnd: variant; sBackup: string; sAction: string);
var
      slSub, slSub_bkp: TStringlist;
      sCheckTime: string;
      aCheckTime: Array Of string;
      i, j, iStart, iEnd, iDuration: integer;
      iDelNext: integer = 0;
      slSub_data: string;
begin

  {convert srt time to second}
  slSub:= TStringList.Create;
  slSub_bkp:= TStringList.Create;
  slSub.LoadFromFile(sTarget,false);
  slSub_bkp.Text:=slSub.Text;

  {convert target time}
  vStart:=StrSec(vStart);
  vEnd:=StrSec(vEnd);
  iDuration:= vEnd - vStart;

  { walk trough SRT, backward for detection an removal }
  j:= 0;
  for i:= slSub.Count -1 downto 0 do
  begin

     if i = iDelNext then begin
         slSub.Delete(i);
         continue;
		 end;

     if slSub.ValueFromIndex[i].Contains(' --> ') then
     begin
         sCheckTime:=slSub.ValueFromIndex[i].Replace(' --> ','-');
         aCheckTime:=sCheckTime.Split('-');

         iStart:= StrSec(aCheckTime[0].Replace(',','.'));
         iEnd:= StrSec(aCheckTime[1].Replace(',','.'));

         { overlap possibilities #1 }
         if iStart < vStart then
            if (iEnd > vStart) or (iEnd > vEnd) then
               begin
               {step forward to look for last empty line}
                j:= i;
                while slSub.ValueFromIndex[j].IsEmpty = false do begin
                      j:= j + 1;
						          end;
                for j:=j downto i do begin
                    slSub.Delete(j);
                end;
                iDelNext:= i - 1;
  				  end;

         { overlap possibilities #1 }
         if iStart > vStart then
            if (iEnd < vEnd) or (iStart < vEnd) then
               begin
                {step forward to look for last empty line}
                j:= i;
                while slSub.ValueFromIndex[j].IsEmpty = false do begin
                      j:= j + 1;
						          end;
                for j:=j downto i do begin
                    slSub.Delete(j);
                end;
                iDelNext:= i - 1;
  				  end;

         iStart:= iStart - iDuration;
         iEnd:= iEnd - iDuration;

         { resync time right before first cell deletion occur }
         if sAction.ToLower = 'crop' then begin
            if iDelNext < 1 then begin
               slSub_Data:=StrSec(iStart) + ' --> ' + StrSec(iEnd);
               slSub_Data:=slSub_Data.Replace('.',',');
               slSub.Delete(i);
               slSub.Insert(i,slSub_Data);
				    end;
         end;

		 end;


     { fix duuble-empty line, if any }
     if (slSub.ValueFromIndex[i].IsEmpty) and (slSub.ValueFromIndex[i - 1].IsEmpty)
     then begin
         slSub.Delete(i);
         continue;
		 end;

	end;

  { walk trough SRT, now forward to sync cells index }
  j:= 1;
  if slSub.ValueFromIndex[0] = '1' then begin
     for i:= 0 to slSub.Count -1 do begin
         if (slSub.ValueFromIndex[i].IsEmpty) and (i + 1 < slSub.Count -1) then begin
            j:=j + 1;
            slSub.Delete(i + 1);
            slSub.Insert(i + 1,IntToStr(j));
				 end;
		 end;
	end
  else StopRun('Subtitle error',1);

  { save backup sub, if needed }
  sBackup:=IncludeTrailingPathDelimiter(sBackup);
  if (sBackup.IsEmpty = false) and DirectoryExists(sBackup) then slSub_bkp.SaveToFile(sBackup + ExtractFileName(sTarget));

  { create final sub }
  slSub.SaveToFile(sTarget);

end;

procedure TSubtcut.ExecOnMKV;
var
  jData: TJSONData;
  jObject : TJSONObject;
  sOut, sTrack, sInt, sTrackName,
  sTrackLang, sTrackDefault: string;
  sExtractSub, sRemoveSub, sAddSub, sTempMKV:  string;
  i: integer;
begin

  {writeln(StrSec(param.vStart));
  writeln(StrSec(param.vEnd));
  halt(0);}

  {get MKV info}
  RunCommandInDir(
                  Param.sTemp,
                  App.sMerge,
                  ['-J',DblQ(Param.sTarget)],
                  sOut,
                  [poNoConsole]);

  jData:= GetJSON(sOut);
  jObject := jData as TJSONObject;
  //writeln(sOut);

  { initial value }
  sExtractSub:=''; sRemoveSub:='!'; sAddSub:='';

  for i:=0 to jObject.GetPath('tracks').Count -1 do
  begin

    sTrack:= jObject.GetPath(MKVSubType.Replace('*',inttostr(i))).AsString;
	        if sTrack.ToLower.Contains('srt') then begin

             {Get necessary info}
              sInt:= IntToStr(i);
              sTrackName:= jObject.GetPath(MKVSubName.Replace('*',sInt)).AsString;
              sTrackLang:= jObject.GetPath(MKVSubLang.Replace('*',sInt)).AsString;
              sTrackDefault:= jObject.GetPath(MKVSubDefault.Replace('*',sInt)).AsString; //<!---- WHERE USE?? @@@@@@@

              sExtractSub:=sExtractSub + sInt +':' + DblQ(Param.sTemp + sTrackName + '.srt') + ' ';
              sRemoveSub:= sRemoveSub + sInt + ',';
              sAddSub:=sAddSub + ' --language 0:'+sTrackLang+' --track-name 0:'+DblQ(sTrackName)+' '+DblQ(sTrackName + '.srt');

					end;
	end;

  {extract sub}
  RunCommandInDir(
      Param.sTemp,
      App.sExtract,
      ['tracks',DblQ(Param.sTarget),sExtractSub],
      sOut,
      [poNoConsole,poWaitOnExit]);
  //writeln(sOut);

  { run eraser on workdir }
  ExecOnDir(Param.sTemp);

  { Prepare variable }
  SetLength(sRemoveSub,Length(sRemoveSub) -1); //--> Remove trailinG comas. Eg 1,2,3, to 1,2,3

  {backup}
  if Param.sBackup.IsEmpty = false then
     CopyFile(Param.sTarget,IncludeTrailingPathDelimiter(Param.sBackup) + ExtractFileName(Param.sTarget));

  {remove old, add new subs}
  sTempMKV:= Rand(7)+'.tmp';
  RunCommandInDir(
      Param.sTemp,
      App.sMerge,
      ['-o',DblQ(sTempMKV),'-s',sRemoveSub,DblQ(Param.sTarget),sAddSub],
      sOut,
      [poNoConsole,poWaitOnExit]);


  { delete temporary files (SRTs, MKV) }
  DeleteFile(Param.sTarget);
  RenameFile(Param.sTemp + sTempMKV,Param.sTarget);
  DeleteDirectory(Param.sTemp,false);

end;

procedure TSubtcut.ExecOnDir(sTargetLC: string);
var
  SRTFiles: TStringList;
  i: integer;
begin
  SRTFiles := TStringList.Create;
  try
    FindAllFiles(SRTFiles, sTargetLC, '*.srt', Param.bRecursive);
    if SRTFiles.Count > 0 then
       begin
        for i:= 0 to SRTFiles.Count -1 do
           ExecOnSrt(SRTFiles.ValueFromIndex[i], Param.vStart, Param.vEnd, Param.sBackup, Param.sAction);
			 end
    else writeln('SRT not found in ' + Param.sTarget);
  finally
    SRTFiles.Free;
  end;

end;

procedure TSubtcut.DoRun;
var
      slParam: TStringlist;
      iCnt: integer;
      //sTarget, sTime, sAction, sBackup: string;
begin

      { Parameters }
	    if ParamCount = 0 then halt(2);

	    slParam:= TStringList.Create;
	    slParam.Sorted:=True;

	    for iCnt:= 1 to ParamCount do
	        slParam.Add(ParamStr(iCnt));

      if (slParam.Text.Contains('help'))
      or (slParam.Text.Contains('-h'))
      or (slParam.Text.Contains('--h'))
      or (slParam.Text.Contains('?'))
      or (slParam.Text.Contains('/?')) then begin
         ShowHelp;
         Terminate;
			end;

      { validating time }
      if (slParam.Values['start'].Contains(':') = false)
      or (slParam.Values['start'].Contains(',') = false)
      then StopRun('Invalid start time',1);

      if (slParam.Values['end'].Contains(':') = false)
      or (slParam.Values['end'].Contains(',') = false)
      then StopRun('Invalid end time',1);

      { remaining params }
      Param.vStart       := slParam.Values['start'].Replace(',','.');
      Param.vEnd         := slParam.Values['end'].Replace(',','.');
      Param.sTarget      := slParam.Values['target'];
      Param.sBackup      := slParam.Values['backup'];
      Param.sAction      := slParam.Values['action'];
      Param.sTemp        := slParam.Values['temp'];
         if Param.sTemp.IsEmpty then
            Param.sTemp:= IncludeTrailingPathDelimiter(GetTempDir) + Rand(7) + DirectorySeparator
         else
            Param.sTemp:= IncludeTrailingPathDelimiter(Param.sTemp) + Rand(7) + DirectorySeparator;
      ForceDirectories(Param.sTemp);

      writeln(param.sAction);
      halt(0);

	    { Show help/hint }
	    if slParam.Find('--help',iCnt) or (Param.sTarget.IsEmpty) then ShowHelp;

	    { Validation: File or path exist }
	    if FileExists(Param.sTarget) = False then
	       if DirectoryExists(Param.sTarget) = False then
	          StopRun('Target file or directory not found', 1);

      if DirectoryExists(Param.sTemp) = False then
         StopRun('Temp directory not found',1);

      if DirectoryExists(Param.sBackup) = False then
         StopRun('Backup directory not found',1);


	    { Check filetype }
	    if (FileGetAttr(Param.sTarget) and faDirectory) <> 0 then
	       ExecOnDir(IncludeTrailingPathDelimiter(Param.sTarget))
	    else
	        if (FileGetAttr(Param.sTarget) and faReadOnly) <> 0 then
	          StopRun('File is read-only', 1)
		      else
		          if ExtractFileExt(Param.sTarget.ToLower) = '.mkv' then
		             ExecOnMKV
		          else
	               if ExtractFileExt(Param.sTarget.ToLower) = '.srt' then ExecOnSRT(Param.sTarget, Param.vStart, Param.vEnd, Param.sBackup, Param.sAction)
	               else ExecOnDir(IncludeTrailingPathDelimiter(ExtractFilePath(Param.sTarget)));



      // stop program loop
      Terminate;
end;

constructor TSubtcut.Create(TheOwner: TComponent);
begin
      inherited Create(TheOwner);
      StopOnException:=True;
end;

destructor TSubtcut.Destroy;
begin
      inherited Destroy;
end;

procedure TSubtcut.ShowHelp;
begin
  Writeln('================================================================================');
  Writeln('SRT Eraser:');
  Writeln('USAGE:');
  Writeln('================================================================================');
  Writeln('target=<path>                            : SRT file/Video File/Directory');
  Writeln('recursive=[yes|*no]                      : Search directory and subdirectory');
  Writeln('start=<hh:mm:ss,mil>                     : Start timeframe (mil = miliseconds ,000)');
  Writeln('end=<hh:mm:ss,mil>                       : End timeframe (mil = miliseconds ,000)');
  Writeln('action=[crop|*erase]                     : "erase": delete, "crop": delete & re-sync');
  Writeln('backup=[path]                            : Original .SRT moved here');
  Writeln('temp=[path]                              : temporary files moved here');
  Writeln('--help                                   : This help');
  Writeln('');
  Writeln('================================================================================');
  halt(0);
end;

begin
      Application:=TSubtcut.Create(nil);
      App.sAppDir:=IncludeTrailingPathDelimiter(ProgramDirectory);
      App.sMerge:=App.sAppDir + 'mkvmerge';
      App.sExtract:=App.sAppDir + 'mkvextract';
      Application.Title:='SRT Eraser';
      Application.Run;
      Application.Free;
end.

